variable "resources" {
  type = list(string)
}

variable "kubeconfig" {
  type = string
}
