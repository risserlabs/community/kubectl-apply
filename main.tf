terraform {
  required_version = ">= 0.13.5"
}

resource "null_resource" "kubectl_apply" {
  for_each = { for value in var.resources : value => value }
  provisioner "local-exec" {
    command     = <<EOF
kubectl --kubeconfig <(echo $KUBECONFIG) \
  apply -f $RESOURCE
EOF
    interpreter = ["sh", "-c"]
    environment = {
      KUBECONFIG = var.kubeconfig
      RESOURCE   = each.value
    }
  }
}
